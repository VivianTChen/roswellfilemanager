#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QStandardPaths>
#include <QDebug>
#include <launcher.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    qmlRegisterType<Launcher>("Launcher", 1, 0, "Launcher");

    QGuiApplication app(argc, argv);


    QQmlApplicationEngine engine;

    //set navigation page param
    if (argc > 1 ) {
        QString navigationPageParam =  QString::fromLocal8Bit(argv[1]);

        qDebug() << "pass parameters: " << navigationPageParam;
        engine.rootContext()->setContextProperty("navigationPageParam", navigationPageParam);
      }
    else {
        engine.rootContext()->setContextProperty("navigationPageParam", nullptr);
    }
    engine.rootContext()->setContextProperty("SystemVideoFolder",
                      QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).first());
    qDebug() << QStandardPaths::standardLocations(QStandardPaths::MoviesLocation);

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
