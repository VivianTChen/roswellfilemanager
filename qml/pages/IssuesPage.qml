import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtGraphicalEffects 1.0
import Qt.labs.folderlistmodel 2.1

//import "../common"

Flickable {
    id: issuesPage
    // index to get access to Loader (Destination)
    //property int myIndex: index
    //contentHeight: root.implicitHeight

    // StackView manages this, so please no anchors here
    // anchors.fill: parent
    property string name: "IssuesPage"
    property string myPath: "/Problem"


    Rectangle {
        id: title
        opacity: 0.7
        height: searchTitleLabel.height + 35
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.margins: 70
        radius: 10

        color: "#5c9fba"
        Text {
            id: searchTitleLabel
            text: "\uf5b1" //"\uf002"
            font.family: "Font Awesome 5 Free"
            color: "white"
            font.bold: true
            font.pointSize: 15
            //anchors.centerIn: parent
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: sortTitleLabel.left
            anchors.rightMargin: 10
        }
        Text {
            id: sortTitleLabel
            visible: false
            text: "\uf3be"
            font.family: "Font Awesome 5 Free"
            color: "white"
            font.bold: true
            font.pointSize: 15
            //anchors.centerIn: parent
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 70

            MouseArea {
                id: sortArea
                anchors.fill: parent
                onClicked: {
                    if (sortTitleLabel.text == "\uf3be")
                        sortTitleLabel.text = "\uf3bf"
                    else
                        sortTitleLabel.text = "\uf3be"
                }
            }
        }
    }

    ListView {
        id: listView
        clip: true
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: title.bottom
        anchors.topMargin: 5
        anchors.margins: 70
        spacing: 5
        add: Transition {
            NumberAnimation { properties: "x"; from: 1000; duration: 500 }
        }


        model: FolderListModel {
            folder: "file://" + SystemVideoFolder + myPath
            showDirs: false
            //nameFilters: ["*.mp4", "*.MP4"]
            sortField: FolderListModel.Time
        }

        delegate: Rectangle {
            opacity: 0.7
            height: fileNameLabel.height + 60
            width: listView.width + 2
            x: -1
            //border.color: Qt.lighter("#67b0d1")
            //border.width: 1
            radius: 10
            color: mArea.pressed ? "#5c9fba" : "#67b0d1"
            /*Image {
                id: picture
                anchors.verticalCenter: parent.verticalCenter
                height: parent.height-10
                 fillMode: Image.PreserveAspectFit
                asynchronous: true
                source: "file://" + model.filePath
            }*/
            Item {
                id: picture
                width: 64
                height: 64
                anchors.verticalCenter: parent.verticalCenter
                //anchors.leftMargin: 25
                Text {
                    anchors.centerIn: parent
                    text: "\uf15c"
                    font.pixelSize: 0.5 * parent.height
                    font.family: "Font Awesome 5 Free"
                    color: "white"
                    opacity: 0.7
                }
            }


            Text {
                id: fileNameLabel
                //anchors.verticalCenter: parent.verticalCenter
                anchors.top: picture.top
                anchors.topMargin: picture.height/4
                anchors.left: picture.right
                anchors.leftMargin: 10

                text: model.fileName

                font.bold: true
                font.pointSize: 12
                color: "white"
                wrapMode: Text.WordWrap

            }

            Text {
                id: detailLabel

                anchors.top: fileNameLabel.bottom
                //anchors.margins: 20
                anchors.left: picture.right
                anchors.leftMargin: 10

                text: model.fileModified + " " + model.fileSize

                font.bold: true
                font.pointSize: 10
                color: "white"
                wrapMode: Text.WordWrap

            }

            MouseArea {
                id: mArea
                anchors.fill: parent
                onClicked: {
                    print ("path: " + model.filePath + " " + model.fileName)
                    //navPane.videoFile = model.filePath
                    //navPane.pushVideoPlayer()
                }
            }        }
    }

    /*
    Pane {
        id: root
        anchors.fill: parent
        ColumnLayout {
            anchors.right: parent.right
            anchors.left: parent.left
            //LabelHeadline {
            Label {
                leftPadding: 100
                text: qsTr("review all files in the problem folder for further processing.")
                font.pixelSize: 20
            }
            //HorizontalDivider {}
            RowLayout {
                //LabelSubheading {
                Label {
                    topPadding: 6
                    leftPadding: 100
                    rightPadding: 10
                    wrapMode: Text.WordWrap
                    text: qsTr(" \n\n")                }
            }

        } // col layout
    } // root
    */

    ScrollIndicator.vertical: ScrollIndicator { }

    // emitting a Signal could be another option
    Component.onDestruction: {
        cleanup()
    }

    // called immediately after Loader.loaded
    function init() {
        console.log("Init done from Issue Page")
    }
    // called from Component.destruction
    function cleanup() {
        console.log("Cleanup done from Issue Page")
    }
} // flickable
