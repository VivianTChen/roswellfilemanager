import QtQuick 2.0
import QtQuick.Controls 2.4
import QtMultimedia 5.9

Item {
    id: videoPlayerPage
    // index to get access to Loader (Destination)
    property int myIndex: index
    //contentHeight: root.implicitHeight

    // StackView manages this, so please no anchors here
    // anchors.fill: parent
    property string name: "VideoPlayerPage"

    /*
    VideoOutput {
        id: video
        width : parent.width
        height : parent.height
        source: mediaPlayer
        property alias mediaSource: mediaPlayer.source
        property alias volume: mediaPlayer.volume
        property bool isRunning: true

        MediaPlayer {
            id: mediaPlayer
            autoPlay: true
            source: "file://" + navPane.videoFile
            volume: 0.5
            //loops: Audio.Infinite
        }

        function play() { mediaPlayer.play() }
        function pause() { mediaPlayer.pause() }
        function stop() { mediaPlayer.stop() }

        function toggleplay() {
            if (isRunning) {
                pause()
                isRunning = false
            } else {
                play()
                isRunning = true
            }
        }

        MouseArea {
            anchors.fill: parent
            onClicked: video.toggleplay()
        }
    }
    */

    Video {
        id: video
        width : parent.width
        height : parent.height
        source: "file://" + navPane.videoFile
        //source: "file:///C:/Users/vivian/sample_iTunes.mov"
        autoPlay: true

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(video.playbackState == MediaPlayer.PlayingState)
                    video.pause()
                else
                    video.play()
            }
        }

        focus: true
        Keys.onSpacePressed: video.playbackState == MediaPlayer.PlayingState ? video.pause() : video.play()
        Keys.onLeftPressed: video.seek(video.position - 5000)
        Keys.onRightPressed: video.seek(video.position + 5000)
    }

    Rectangle {
        id: sliderBar
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        height: 50
        color: primaryColor
        opacity: 0.7

        Label {
            id: positionLabel
            anchors.right: seekSlider.left
            anchors.rightMargin: 10
            height: parent.height
            text: video.position
            verticalAlignment:Text.AlignVCenter
        }

        Slider {
            id: seekSlider
            anchors.centerIn: parent
            width: parent.width * 0.675
            value: video.position // for slider to move along with movie
            from: 0
            to: video.duration

            onPressedChanged: {
                video.seek(seekSlider.value)
            }
        }


        Label {
            id: durationLabel
            anchors.left: seekSlider.right
            anchors.leftMargin: 10
            height: parent.height
            text: video.duration
            verticalAlignment:Text.AlignVCenter
        }

    }

    // emitting a Signal could be another option
    Component.onDestruction: {
        cleanup()
    }


    // called immediately after Loader.loaded
    function init() {
        console.log("Init done from VideoPlayerPage")
    }
    // called from Component.destruction
    function cleanup() {
        console.log("Cleanup done from VideoPlayerPage")
    }
}
