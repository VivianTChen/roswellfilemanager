#include "launcher.h"

Launcher::Launcher(QObject *parent) :
    QObject(parent),
    m_process(new QProcess(this))
{
}
QString Launcher::launch(const QString &program, const QVariantList &arguments)
{
    QStringList args;

    // convert QVariantList from QML to QStringList for QProcess

    for (int i = 0; i < arguments.length(); i++)
        args << arguments[i].toString();

    m_process->start(program, args);
    //m_process->waitForFinished(-1);
    m_process->waitForStarted(-1);
    //QByteArray bytes = m_process->readAllStandardOutput();
    //QString output = QString::fromLocal8Bit(bytes);
    //return output;
    return nullptr;
}
Launcher::~Launcher() {
}
