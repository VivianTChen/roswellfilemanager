// ekke (Ekkehard Gentz) @ekkescorner
import QtQuick 2.11
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.4

//import "../common"

ToolBar {
    id: myTitleBar

    RowLayout {
        focus: false
        spacing: 6
        anchors.fill: parent
        Item {
            width: 4
        }

        ToolButton {
            visible: !backButton.visible
            focusPolicy: Qt.NoFocus

            Text {
                anchors.centerIn: parent
                text: "\uf0c9"
                font.pixelSize: 0.6 * parent.height
                font.family: "Font Awesome 5 Free"
                color: Material.accent
            }
            onClicked: {
                appWindow.openNavigationBar()
            }
        } // menu button
        // F A K E
        // fake button to avoid flicker and repositioning of titleLabel
        ToolButton {
            visible: false //!backButton.visible && appWindow.hasOnlyOneMenu && !appWindow.isLandscape
            enabled: false
            focusPolicy: Qt.NoFocus
        } // fake button
        ToolButton {
            id: backButton
            focusPolicy: Qt.NoFocus
            visible: navigationModel[navigationIndex].canGoBack && destinations.itemAt(navigationIndex).item.depth > 1 // && initDone
            Text {
                anchors.centerIn: parent
                text: "\uf100"
                font.pixelSize: 0.6 * parent.height
                font.family: "Font Awesome 5 Free"
                color: Material.accent
            }
            onClicked: {

                destinations.itemAt(navigationIndex).item.goBack()
            }
        }

        //LabelTitle {
        Label {
            id: titleLabel
            Layout.fillWidth: true
            text: currentTitle
            leftPadding: 6
            rightPadding: 6
            elide: Label.ElideRight
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
            //color: textOnPrimary
        }
        ToolButton {
            focusPolicy: Qt.NoFocus
            enabled: false
        }
    } // end RowLayout
} // end ToolBar


