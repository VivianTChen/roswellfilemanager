import QtQuick 2.11
import QtQuick.Window 2.2
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4

import Launcher 1.0


import "navigation"

ApplicationWindow {
    id: appWindow
    visible: true
    width: 800
    height: 480
    title: qsTr("Roswell File Manager")

    readonly property var fontAwesome: FontLoader {
        name: "fontawesome"
        source: "qrc:/images/fa-solid-900.ttf"
    }

    property color primaryColor: Material.primary
    property color dividerColor: "white"
    property alias navigationBar: drawerLoader.item

    property var activationPolicy: {
        "NONE":0, "IMMEDIATELY":1, "LAZY":2, "WHILE_CURRENT":3
    }

    // NAVIGATION BAR PROPRTIES (a_p == activationPolicy)
    // IMMEDIATELY: Home
    // LAZY: customer, orders
    // WHILE_CURRENT: About, Settings
    // StackView: Home --> QtPage, Settings --> primary / Accent
    property var navigationModel: [
        {"type": "../navigation/DrawerDivider.qml", "name": "", "icon": "", "source": "", "a_p":1, "canGoBack":false},
        {"type": "../navigation/DrawerNavigationButton.qml", "name": "Evidence", "icon": "\uf1c8", "source": "../pages/EvidencePage.qml", "showCounter":true, "showMarker":true, "a_p":1, "canGoBack":false},
        {"type": "../navigation/DrawerNavigationButton.qml", "name": "Background", "icon": "\uf008", "source": "../navigation/BackgroundNavigation.qml", "showCounter":true, "showMarker":true, "a_p":2, "canGoBack":true},
        {"type": "../navigation/DrawerNavigationButton.qml", "name": "Bodycamera", "icon": "\uf083", "source": "../pages/BodycameraPage.qml", "showCounter":true, "showMarker":true, "a_p":2, "canGoBack":true},
        {"type": "../navigation/DrawerNavigationButton.qml", "name": "Problem", "icon": "\uf1c3", "source": "../pages/IssuesPage.qml", "showCounter":true, "showMarker":true, "a_p":2, "canGoBack":true},
        {"type": "../navigation/DrawerDivider.qml", "name": "", "icon": "", "source": "", "a_p":1, "canGoBack":false},
        {"type": "../navigation/DrawerNavigationButton.qml", "name": "Settings", "icon": "\uf013", "source": "../pages/SettingsPage.qml", "showCounter":false, "showMarker":false, "a_p":3, "canGoBack":true},
        {"type": "../navigation/DrawerNavigationButton.qml", "name": "About", "icon": "", "source": "../pages/EvidencePage2.qml", "showCounter":false, "showMarker":false, "a_p":3, "canGoBack":false}
    ]

    property var navigationTitles: [
        "",
        qsTr("Evidence Viewer"),
        qsTr("Background File Viewer"),
        qsTr("Body Warn Camera Evidence Viewer"),
        qsTr("Problem Folder"),
        "",
        qsTr("File Manager Configuration"),
        qsTr("Roswell File Manager")
    ]
    property string currentTitle: navigationTitles[navigationIndex]

    property var navigationData: [
        {},
        {"counter":5, "marker":"yellow"},
        {"counter":6, "marker":"blue"},
        {"counter":7, "marker":"orange"},
        {"counter":0, "marker":"red"},
        {},
        {"counter":0, "marker":"transparent"},
        {"counter":0, "marker":"transparent"}
    ]

    property int firstActiveDestination: 0
    property int navigationIndex: firstActiveDestination
    onNavigationIndexChanged: {
        rootPane.activateDestination(navigationIndex)
    }

    header:titleBar

    Loader {
        id: titleBar
        visible: true //(!isLandscape || useDefaultTitleBarInLandscape) && initDone
        active: true //(!isLandscape || useDefaultTitleBarInLandscape) && initDone
        source: "navigation/DrawerTitleBar.qml"
    }

    Launcher {
            id:playerlauncher
        }

    // NAVIGATION BARS (DRAWER and FAVORITES)
    // The sliding Drawer
    // there's an alias in appWindow: navigationBar --> drawerLoader.item
    Loader {
        id: drawerLoader
        active: true //initDone
        visible: true // initDone
        source: "navigation/DrawerNavigationBar.qml"
    }

    // the ROOT contains always only one Page,
    // which will be replaced if root node changed
    StackView {
        id: rootPane
        focus: true
        anchors {
            top: titleBar.bottom
            left: parent.left
            topMargin: 6
            right: parent.right
            bottom: parent.bottom
        }

        // shows a Busy indicator - probably not noticed yet
        // but in real life app loading first Page or Pane could took some time if heavy
        Loader {
            id: initialPlaceholder
            source: "pages/InitialItemPage.qml"
            active: true
            visible: false
            onLoaded: {
                rootPane.initialItem = item
                item.init()
            }
        }

        replaceEnter: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 0
                to:1
                duration: 300
            }
        }
        replaceExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to:0
                duration: 300
            }
        }

        Repeater {
            id: destinations
            model: navigationModel
            // PageLoader
            // depends from activationPolicy how to load dynamically
            PageLoader {
                id: destinationLoader
            }
            Component.onCompleted: {
                // all destinationLoader created
                // all destinatation items w activationPolicy IMMEDIATELY activated
                // now show first destination (should always be IMMEDIATELY)

                if(navigationPageParam && navigationPageParam === "Background") {
                    firstActiveDestination = 2
                }
                else {
                    firstActiveDestination = 1
                }
            }

        }

        // switch to new Destination
        // Destinations are lazy loaded via Loader
        function activateDestination(navigationIndex) {
            if(destinations.itemAt(navigationIndex).status == Loader.Ready) {
                console.log("replace item on root stack: "+navigationIndex)
                replaceDestination(destinations.itemAt(navigationIndex))
            } else {
                console.log("first time item to be replaced: "+navigationIndex)
                destinations.itemAt(navigationIndex).active = true
            }
        }

        function replaceDestination(theItemLoader) {
            var previousIndex = rootPane.currentItem.currentIndex
            var previousItemLoader
            if(previousIndex >= 0) {
                previousItemLoader  = destinations.itemAt(previousIndex)
            }

            // now replace the Page
            rootPane.replace(theItemLoader.item)
            // check if previous should be unloaded

            if(previousIndex >= 0) {
                if(destinations.itemAt(previousIndex).pageActivationPolicy == activationPolicy.WHILE_CURRENT) {
                    destinations.itemAt(previousIndex).active = false
                }
            } else {
                initialPlaceholder.active = false
            }
        }

    }

    function openNavigationBar() {
        navigationBar.open()
    }
    function closeNavigationBar() {
        navigationBar.close()
    }

    function launchVideoPlayer(fileName) {
         //if (Qt.platform.os === "windows" ) {
         //   playerlauncher.launch("c:\\Program Files (x86)\\Windows Media Player\\wmplayer.exe");
         //}
         //else {
            playerlauncher.launch("RoswellVideoPlayer", [fileName]);
         //}
    }

}
