import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.5
import QtGraphicalEffects 1.0
import Qt.labs.folderlistmodel 2.1

import "../common"

Flickable {
    id: backgroundPage
    // index to get access to Loader (Destination)
    //property int myIndex: index
    //contentHeight: listView.implicitHeight


    // StackView manages this, so please no anchors here
    // anchors.fill: parent
    property string name: "Background"
    property string myPath: "/Background"
    property bool allLocked: false


    Rectangle {
        id: title
        opacity: 0.7
        height: searchTitleLabel.height + 35
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.margins: 70
        radius: 10

        color: "#5c9fba"

        /*Label {
            id: dateTimeLabel
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 15
            text: qsTr("Date Time")

        }*/
        ComboBox {
            id: dateCombo
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left //dateTimeLabel.right
            anchors.leftMargin: 15
            width: parent.width * 0.3
            model: ["Select Date",
                "1/7/2020 ",
                "1/6/2020",
                "1/5/2020 ",
                "1/4/2020",
                "1/3/2020 ",
                "1/2/2020",
                "/1/1/2020"]
        }

        ComboBox {
            id: timeCombo
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: dateCombo.right
            anchors.leftMargin: 5
            width: parent.width * 0.3
            model: ["Select time",
                "00:00 - 06:00",
                "06:00 - 12:00",
                "12:00 - 18:00",
                "18:00 - 24:00"]
        }
        Text {
            id: searchTitleLabel
            text: "\uf002"
            font.family: "Font Awesome 5 Free"
            color: "white"
            font.bold: true
            font.pointSize: 15
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: timeCombo.right
            anchors.leftMargin: 15
        }

        CircleButton {
            id: offloadAllButton
            borderWidth: 1
            height: parent.height * 0.625
            width: height
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: lockAllButton.left
            anchors.rightMargin: 35

            text: "\uf093"
            font.pixelSize: 15
            font.family: "Font Awesome 5 Free"
            bgColor: "transparent"
            onClicked: {
                //offload all file
            }
        }


        CircleButton {
            id: lockAllButton
            borderWidth: 1
            height: parent.height * 0.625
            width: height
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 25
            text: checked? "\uf13e" : "\uf023"
            font.pixelSize: 15
            font.family: "Font Awesome 5 Free"
            bgColor: "transparent"
            onClicked: {
                allLocked = checked
            }
        }

    }

    ListView {
        id: listView
        clip: true
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: title.bottom
        anchors.topMargin: 5
        anchors.margins: 70
        spacing: 5
        add: Transition {
            NumberAnimation { properties: "x"; from: 1000; duration: 500 }
        }

        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds

        model: FolderListModel {
            folder: "file://" + SystemVideoFolder + myPath
            showDirs: false
            nameFilters: ["*.mp4", "*.wmv"]
            sortField: FolderListModel.Time
        }

        delegate: Rectangle {
            opacity: 0.7
            height: fileNameLabel.height + 60
            width: listView.width + 2
            x: -1
            //border.color: Qt.lighter("#67b0d1")
            //border.width: 1
            radius: 10
            color: mArea.pressed ? "#5c9fba" : "#67b0d1"
            /*Image {
                id: picture
                anchors.verticalCenter: parent.verticalCenter
                height: parent.height-10
                 fillMode: Image.PreserveAspectFit
                asynchronous: true
                source: "file://" + model.filePath
            }*/
            Item {
                id: picture
                width: 64
                height: 64
                anchors.verticalCenter: parent.verticalCenter
                //anchors.leftMargin: 25
                Text {
                    anchors.centerIn: parent
                    text: "\uf1c8"
                    font.pixelSize: 0.5 * parent.height
                    font.family: "Font Awesome 5 Free"
                    color: "white"
                    opacity: 0.7
                }
            }


            Text {
                id: fileNameLabel
                //anchors.verticalCenter: parent.verticalCenter
                anchors.top: picture.top
                anchors.topMargin: picture.height/4
                anchors.left: picture.right
                anchors.leftMargin: 10

                text: model.fileName

                font.bold: true
                font.pointSize: 12
                color: "white"
                wrapMode: Text.WordWrap

            }

            Text {
                id: detailLabel

                anchors.top: fileNameLabel.bottom
                //anchors.margins: 20
                anchors.left: picture.right
                anchors.leftMargin: 10

                text: model.fileModified + " " + model.fileSize

                font.bold: true
                font.pointSize: 10
                color: "white"
                wrapMode: Text.WordWrap

            }

            MouseArea {
                id: mArea
                anchors.fill: parent
                onClicked: {
                    print ("path: " + model.filePath + " " + model.fileName)
                    navPane.videoFile = model.filePath
                    navPane.pushVideoPlayer()

                    //launchVideoPlayer(model.filePath);
                }
            }

            RoundButton {
                id: uploadButton
                height: parent.height * 0.8
                width: height
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: lockButton.left
                anchors.rightMargin: 10
                checkable: true

                text: "\uf093"
                font.pixelSize: 16
                font.family: "Font Awesome 5 Free"
                onClicked: {
                    if(checked)
                       enabled = false
                }
            }

            RoundButton {
                id: lockButton
                height: parent.height * 0.8
                width: height
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: 10
                checkable: true
                checked: allLocked
                text: checked ? "\uf13e" : "\uf023"
                font.pixelSize: 16
                font.family: "Font Awesome 5 Free"
            }

        }

        ScrollBar.vertical: ScrollBar {
            width: 5
            policy: ScrollBar.AlwaysOn
        }
    }

    // emitting a Signal could be another option
    Component.onDestruction: {
        cleanup()
    }

    // called immediately after Loader.loaded
    function init() {
        console.log("Init done from Background Page")
    }
    // called from Component.destruction
    function cleanup() {
        console.log("Cleanup done from Background Page")
    }
} // flickable
