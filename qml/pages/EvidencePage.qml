import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtGraphicalEffects 1.0

//import "../common"

Flickable {
    id: evidencePage
    // index to get access to Loader (Destination)
    //property int myIndex: index
    //contentHeight: root.implicitHeight

    // StackView manages this, so please no anchors here
    // anchors.fill: parent
    property string name: "EvidencePage"

    property var transferStatus: {
        "READY":0, "INPROCESS":1, "DONE":2, "FAILED":3, "NEXT":4, "HOLD":5
    }

    ListView {
        id: listView
        clip: true
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: parent.top //title.bottom
        anchors.topMargin: 5
        anchors.margins: 70
        spacing: 5
        add: Transition {
            NumberAnimation { properties: "x"; from: 1000; duration: 500 }
        }

        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds

        ListModel {
            id: videoListModel

            ListElement {
                filePath: "/home/georges/Videos/BWC/00000000000DCW003@20170102011546_0001_SmallLen.MP4"
                picturePath: "../../images/image1.jpg"
                fileModified: "13.04.32 2019.04.21"
                videoDuration: 34
                eventType: "Speeding"
                status:"READY"
            }

            ListElement {
                filePath: "/home/georges/Videos/BWC/00000000000DCW003@20170102011546_0001_SmallLen.MP4"
                picturePath: "../../images/image4.jpg"
                fileModified: "13.03.30 2019.04.21"
                videoDuration: 75
                eventType: "Red light"
                status:"READY"
            }
            ListElement {
                filePath: "/home/georges/Videos/BWC/00000000000DCW003@20170102011546_0001_SmallLen.MP4"
                picturePath: "../../images/image1.jpg"
                fileModified: "13.04.32 2019.04.21"
                videoDuration: 34
                eventType: "Speeding"
                status:"INPROCESS"
            }

            ListElement {
                filePath: "/home/georges/Videos/BWC/00000000000DCW003@20170102011546_0001_SmallLen.MP4"
                picturePath: "../../images/image4.jpg"
                fileModified: "13.03.30 2019.04.21"
                videoDuration: 75
                eventType: "Red light"
                status:"FAILED"
            }

            ListElement {
                filePath: "/home/georges/Videos/BWC/00000000000DCW003@20170102011546_0001_SmallLen.MP4"
                picturePath: "../../images/image4.jpg"
                fileModified: "13.03.30 2019.04.21"
                videoDuration: 75
                eventType: "Red light"
                status:"DONE"
            }

        }
        model: videoListModel
        /*{
            folder: "file://" + SystemVideoFolder + myPath
            showDirs: false
            //nameFilters: ["*.mp4", "*.MP4"]
            sortField: FolderListModel.Time
        }*/

        delegate: Rectangle {
            opacity: 0.7
            height: fileNameLabel.height + 60
            width: listView.width + 2
            x: -1
            //border.color: Qt.lighter("#67b0d1")
            //border.width: 1
            //radius: 10
            color: mArea.pressed ? "#5c9fba" : "#67b0d1"
            Image {
                id: picture
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 10
                height: parent.height-10
                fillMode: Image.PreserveAspectFit
                asynchronous: true
                source: model.picturePath //"file://" + model.filePath

                Text {
                    anchors.centerIn: parent
                    text: "\uf04b" //"\uf144"
                    font.pixelSize: 0.7 * parent.height
                    font.family: "Font Awesome 5 Free"
                    color: "white"
                    opacity: 0.6
                }

                MouseArea {
                    id: mArea
                    anchors.fill: parent
                    onClicked: {
                        print ("path: " + model.filePath)
                        //navPane.videoFile = model.filePath
                        //navPane.pushVideoPlayer()

                        launchVideoPlayer(model.filePath);
                    }
                }
            }
            /*Item {
                id: picture
                width: 64
                height: 64
                anchors.verticalCenter: parent.verticalCenter
                //anchors.leftMargin: 25
                Text {
                    anchors.centerIn: parent
                    text: "\uf15c"
                    font.pixelSize: 0.5 * parent.height
                    font.family: "Font Awesome 5 Free"
                    color: "white"
                    opacity: 0.7
                }
            }*/


            Text {
                id: fileNameLabel
                //anchors.verticalCenter: parent.verticalCenter
                anchors.top: picture.top
                anchors.topMargin: picture.height/4
                anchors.left: picture.right
                anchors.leftMargin: 10

                text: model.fileModified

                font.bold: true
                font.pointSize: 12
                color: "white"
                wrapMode: Text.WordWrap

            }

            Text {
                id: detailLabel

                anchors.top: fileNameLabel.bottom
                //anchors.margins: 20
                anchors.left: picture.right
                anchors.leftMargin: 10

                text: model.eventType + " | " + model.videoDuration + "mns"

                font.bold: true
                font.pointSize: 10
                color: "white"
                wrapMode: Text.WordWrap

            }

            Text {
                id: statusLabel
                anchors.verticalCenter: parent.verticalCenter
                //anchors.top: picture.top
                //anchors.topMargin: picture.height/2
                anchors.right: parent.right //nextButton.left
                anchors.rightMargin: 10

                text: model.status

                font.bold: true
                font.pointSize: 12
                color: "white"
                wrapMode: Text.WordWrap

            }

            RoundButton {
                id: nextButton
                visible: (model.status != "DONE" && model.status != "INPROCESS")
                height: parent.height * 0.8
                width: height
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: holdButton.left
                anchors.rightMargin: 10
                checkable: true

                text: checked? "\uf0e2" : "\uf093"
                font.pixelSize: 16
                font.family: "Font Awesome 5 Free"
                onClicked: {
                    if(checked)
                        model.status = "NEXT"
                    else
                       model.status = "READY"
                }
            }

            RoundButton {
                id: holdButton
                visible: (model.status != "DONE" && model.status != "INPROCESS")
                height: parent.height * 0.8
                width: height
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: statusLabel.left // parent.right
                anchors.rightMargin: 10
                checkable: true
                text: checked? "\uf0e2" : "\uf715"
                font.pixelSize: 16
                font.family: "Font Awesome 5 Free"
                //onClicked: {checked = !checked}
            }

        }

        ScrollBar.vertical: ScrollBar {
            width: 5
            policy: ScrollBar.AlwaysOn
        }

    }

    /*Pane {
        id: root
        anchors.fill: parent
        ColumnLayout {
            anchors.right: parent.right
            anchors.left: parent.left
            //LabelHeadline {
            Label {            
                leftPadding: 200
                text: qsTr("Event/Incident listed by chronological order")
                font.pixelSize: 20
            }
            //HorizontalDivider {}
            RowLayout {
                //LabelSubheading {
                Label {
                    topPadding: 6
                    leftPadding: 200
                    rightPadding: 10
                    wrapMode: Text.WordWrap
                    text: qsTr("with the newest one on the top. \n\n")                }
            }

        } // col layout
    } // root
    */

    ScrollIndicator.vertical: ScrollIndicator { }

    // emitting a Signal could be another option
    Component.onDestruction: {
        cleanup()
    }

    // called immediately after Loader.loaded
    function init() {
        console.log("Init done from Evidence page")
    }
    // called from Component.destruction
    function cleanup() {
        console.log("Cleanup done from Evidence page")
    }
} // flickable
