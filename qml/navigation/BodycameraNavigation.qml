import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtGraphicalEffects 1.0

import "../pages"

Item {
    id: navPage
    property alias depth: navPane.depth
    property string name: "BodycameraNavPage"

    // index to get access to Loader (Destination)
    //property int myIndex: index

    StackView {
        id: navPane
        anchors.fill: parent
        property string name: "BodycameraNavPage"
        property string videoFile: ""
        focus: true

        initialItem: BodycameraPage{}

        Loader {
            id: videoPlayerPageLoader
            active: false
            visible: false
            source: "../pages/VideoPlayerPage.qml"
            onLoaded: {
                navPane.push(item)
                item.init()
            }
        }

        function pushVideoPlayer() {
            videoPlayerPageLoader.active = true
        }

        function popOnePage() {
            var page = pop()
            if(page.name == "VideoPlayerPage") {
                videoPlayerPageLoader.active = false
                return
            }
        }

    }


    // triggered from BACK KEYs:
    // Back Button from TitleBar
    function goBack() {
        // check if goBack is allowed
        //
        navPane.popOnePage()
    }

    Component.onDestruction: {
        cleanup()
    }

    function init() {
        console.log("INIT BodycameraNavPage")
    }
    function cleanup() {
        console.log("CLEANUP BodycameraNavPage")
    }

}

