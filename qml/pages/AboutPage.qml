import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtGraphicalEffects 1.0

//import "../common"

Flickable {
    id: aboutPage
    // index to get access to Loader (Destination)
    property int myIndex: index
    contentHeight: root.implicitHeight

    // StackView manages this, so please no anchors here
    // anchors.fill: parent
    property string name: "About"

    Pane {
        id: root
        anchors.fill: parent
        ColumnLayout {
            anchors.right: parent.right
            anchors.left: parent.left
            //LabelHeadline {
            Label {
                leftPadding: 100
                text: qsTr("About Roswell File Manager")
                font.pixelSize: 20
            }
            //HorizontalDivider {}
            RowLayout {
                //LabelSubheading {
                Label {
                    topPadding: 6
                    leftPadding: 100
                    rightPadding: 10
                    wrapMode: Text.WordWrap
                    text: qsTr("The file manager provide access to all data files. \n\n")                }
            }

        } // col layout
    } // root
    ScrollIndicator.vertical: ScrollIndicator { }

    // emitting a Signal could be another option
    Component.onDestruction: {
        cleanup()
    }

    // called immediately after Loader.loaded
    function init() {
        console.log("Init done from ABOUT")
    }
    // called from Component.destruction
    function cleanup() {
        console.log("Cleanup done from ABOUT")
    }
} // flickable
